# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Yes         |
| Text input                                       | 10%       | Yes         |
| Cursor icon                                      | 10%       | Yes         |
| Refresh button                                   | 10%       | Yes         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Yes         |
| Un/Re-do button                                  | 10%       | Yes         |
| Image tool                                       | 5%        | Yes         |
| Download                                         | 5%        | Yes         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | None         |


---

### How to use 

    The website provides a drawing canvas that has multiple drawing functions such as drawing with brush, or drawing shapes, and even inserting text to the canvas or uploading your own picture to the canvas.
    
| **Functions**               | **Usage**|
| :----------------------------------------------- | :----------------: |
| Brush button     | Draws a line wherever you drag your mouse to in the canvas |
| Eraser button     | Erases anything drawn on the canvas |
| Text button | Creates a textbox where we can type text and print it to the canvas |
| Line button | Draws a straight line |
| Square button | Draws a square in the canvas |
| Circle button | Draws a circle in the canvas |
| Triangle button | Draws a triangle in the canvas|
| Undo button | Return one step to the previously saved canvas on each press |
| Redo button | Return back to the saved canvas before you press undo on each press|
| Reset button | Clears the canvas to a blank canvas |
| Download button | Download the currently saved canvas |
| Upload button | Uploads images from your local folder and fits it to the canvas |
| Color picker | Chooses the color you want to be applied when drawing |
| Brush size | Chooses the thickness of your brush |
| Font | Chooses the font you like when using text button|
| Font size| Chooses the font size you want when using text button|



### Bonus description

    No bonus done

    
### Gitlab page link

[https://107062382.gitlab.io/AS_01_WebCanvas](https://107062382.gitlab.io/AS_01_WebCanvas)

### Others (Optional)
**Suggestion** <br>
Please provide more information or tutorial or a template for the projects rather than just showing the target and let the students make the project from scratch. It takes a lot of time to finish the project. Some students might also have another assignments or tests that needs study as well, but can't since making the project requires an unbelieveably long time to finisih.
    
    