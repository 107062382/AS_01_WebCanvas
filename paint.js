var canvas = document.getElementById("myCanvas");
var tool = "brush";
var ctx = canvas.getContext("2d");
var laststep = [];
var step = -1;

var painting = false;
var storeImg = new Image();
var xstart = 0;
var ystart = 0;
var xPos, yPos;
var endx, endy;
var coorX;
var coorY;

var bodyCursor = document.getElementById("mainbody");
var uploadbutton = document.getElementById("Upload").addEventListener('change', handleImage, false);
var currentcolor = document.getElementById("colorbox");
var currentsize = document.getElementById("brushsize");
var insertText = document.getElementById("realtext");

function textCoor(e) {
    coorX = e.offsetX + 300;
    coorY = e.offsetY + 110;
}

window.addEventListener("mousemove", textCoor);

function cross() {
    bodyCursor.style.cursor = "crosshair";
}
function brushcursor() {
    bodyCursor.style.cursor = "url('brushcurs.png') 0 20, auto";
}
function erasecursor() {
    bodyCursor.style.cursor = "url('erasercurs.png') 0 20, auto";
}
function textcursor() {
    bodyCursor.style.cursor = "text"; 
}

var brushbutton = document.getElementById("Brush").addEventListener('click', function() {
    tool = "brush";
    brushcursor();
    insertText.style.setProperty("left", -300+"px");
    insertText.style.setProperty("top", -300+"px");
    insertText.value = "";
});
var eraserbutton = document.getElementById("Eraser").addEventListener('click', function() {
    tool = "eraser";
    erasecursor();
    insertText.style.setProperty("left", -300+"px");
    insertText.style.setProperty("top", -300+"px");
    insertText.value = "";
});
var textbutton = document.getElementById("Text").addEventListener('click', function() {
    tool = "textinp";
    textcursor();
    insertText.value = "";
});
var linebutton = document.getElementById("Line").addEventListener('click', function() {
    tool = "line";
    cross();
    insertText.style.setProperty("left", -300+"px");
    insertText.style.setProperty("top", -300+"px");
    insertText.value = "";
});
var squarebutton = document.getElementById("Square").addEventListener('click', function() {
    tool = "square";
    cross();
    insertText.style.setProperty("left", -300+"px");
    insertText.style.setProperty("top", -300+"px");
    insertText.value = "";
});
var circlebutton = document.getElementById("Circle").addEventListener('click', function() {
    tool = "circle";
    cross();
    insertText.style.setProperty("left", -300+"px");
    insertText.style.setProperty("top", -300+"px");
    insertText.value = "";
});
var trianglebutton = document.getElementById("Triangle").addEventListener('click', function() {
    tool = "triangle";
    cross();
    insertText.style.setProperty("left", -300+"px");
    insertText.style.setProperty("top", -300+"px");
    insertText.value = "";
});
var undobutton = document.getElementById("Undo").addEventListener('click', function() {
    undo();
    bodyCursor.style.cursor = "default";
    insertText.style.setProperty("left", -300+"px");
    insertText.style.setProperty("top", -300+"px");
    insertText.value = "";
});
var redobutton = document.getElementById("Redo").addEventListener('click', function() {
    redo();
    bodyCursor.style.cursor = "default";
    insertText.style.setProperty("left", -300+"px");
    insertText.style.setProperty("top", -300+"px");
    insertText.value = "";
});
var resetbutton = document.getElementById("Reset").addEventListener('click', function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    bodyCursor.style.cursor = "default";
    insertText.style.setProperty("left", -300+"px");
    insertText.style.setProperty("top", -300+"px");
    insertText.value = "";
});
var downloadbutton = document.getElementById("Download").addEventListener('click', function(e) {
    var img = canvas.toDataURL("image/png");
    var link = document.createElement('a');
    link.download = "my-image.png";
    link.href = img;
    link.click();
    bodyCursor.style.cursor = "default";
    insertText.style.setProperty("left", -300+"px");
    insertText.style.setProperty("top", -300+"px");
    insertText.value = "";
});

var startpos = function(e) {
    painting = true;
    xstart = xPos;
    ystart = yPos;
    storeImg.src = canvas.toDataURL();
}

var endpos = function(e) {
    painting = false;
    ctx.beginPath();
    storesteps();
}

var draw = function(e) {
    let canvasSizeData = canvas.getBoundingClientRect();
    ctx.strokeStyle = currentcolor.value;
    ctx.lineWidth = currentsize.value/2;
    xPos = (e.clientX - canvasSizeData.left) * (canvas.width  / canvasSizeData.width);
    yPos = (e.clientY - canvasSizeData.top)  * (canvas.height / canvasSizeData.height);
    endx = xPos - xstart;
    endy = yPos - ystart;
    if (painting) {
        if(tool == "brush") {
            ctx.lineTo(xPos, yPos);
            ctx.stroke();
            ctx.beginPath();
            //context.arc(xPos, yPos, ctx.lineWidth, 0, Math.PI*2);
            ctx.moveTo(xPos, yPos);
            ctx.lineCap = "round";
            ctx.lineJoin = "round";
            ctx.globalCompositeOperation = "source-over";
        }
        else if (tool == "eraser") {
            ctx.beginPath();
            ctx.globalCompositeOperation = "destination-out";
            ctx.arc(xPos,yPos,8,0,Math.PI*2,false);
            ctx.fill();
        }
        else if (tool == "line") {
            canvas.width = canvas.width;
            ctx.strokeStyle = currentcolor.value;
            ctx.lineWidth = currentsize.value/2;
            ctx.drawImage(storeImg, 0, 0);
            ctx.beginPath();
            ctx.moveTo(xPos,yPos);
            ctx.lineTo(xstart,ystart);
            ctx.stroke();
            ctx.closePath();
        }
        else if (tool == "square") {
            canvas.width = canvas.width;
            ctx.strokeStyle = currentcolor.value;
            ctx.lineWidth = currentsize.value/2;
            ctx.drawImage(storeImg, 0, 0);
            ctx.strokeRect(xstart,ystart,endx,endy);
        }
        else if (tool == "circle") {
            canvas.width = canvas.width;
            ctx.strokeStyle = currentcolor.value;
            ctx.lineWidth = currentsize.value/2;
            ctx.drawImage(storeImg, 0, 0);
            ctx.arc(xstart,ystart,Math.sqrt(Math.pow(endx,2)+Math.pow(endy,2)),0,2*Math.PI);
            ctx.stroke();
        }
        else if (tool == "triangle") {
            canvas.width = canvas.width;
            ctx.strokeStyle = currentcolor.value;
            ctx.lineWidth = currentsize.value/2;
            ctx.drawImage(storeImg, 0, 0);
            ctx.beginPath();
            ctx.moveTo(xstart + (endx/2), ystart);
            ctx.lineTo(xPos, yPos);
            ctx.lineTo(xPos - endx, yPos);
            ctx.closePath();
            ctx.stroke();
        }
        else if(tool == "textinp") {

            /*var mouseX = xPos + 100;
            var mouseY = yPos + 60;
            endx = mouseX - 100;
            endy = mouseY - 50;*/

            insertText.style.setProperty("left", coorX+"px");
            insertText.style.setProperty("top", coorY+"px");
            
        }
    }
}

document.onkeydown = function(e) {
    var key = e.keyCode;
    if(tool == "textinp") {
        var Size = document.getElementById("picksize").value;
        var fontType = document.getElementById("pickfont").value;
        if (key == 13) {
            var String = insertText.value;
            ctx.font =  Size + "px " + fontType;
            ctx.fillStyle = currentcolor.value;
            ctx.strokeStyle = currentcolor.value;
            ctx.fillText(String, xstart, ystart + 5, 1000);
            insertText.style.setProperty("left", -300+"px");
            insertText.style.setProperty("top", -300+"px");
            insertText.value = "";
            storesteps();
        }
    }
}

function storesteps () {
    step++;
    laststep.length = step + 1;
    laststep[step] = canvas.toDataURL();
}

var canvasPic;

function undo() {
    if (step > 0) {
        step--;
        canvasPic = new Image();
        canvasPic.src = laststep[step];
        canvasPic.onload = function () {
            canvas.height = canvas.height;
            ctx.drawImage(canvasPic, 0, 0);
        }
    }
    else {
        step = -1;
        canvas.height = canvas.height;
    } 
}

function redo() {
    if (step < laststep.length - 1) {
        step++;
        canvasPic = new Image();
        canvasPic.src = laststep[step];
        canvasPic.onload = function () {
            canvas.height = canvas.height;
            ctx.drawImage(canvasPic, 0, 0);
        }  
    }
}

/*function sizechange (e) {
    ctx.lineWidth = currentsize.value/2;
}*/

var imag;
var reader;

function handleImage(e){
    reader = new FileReader();
    reader.onload = function(event){
        imag = new Image();
        imag.onload = function(){
            canvas.width = imag.width;
            canvas.height = imag.height;
            ctx.drawImage(imag,0,0);
        }
        imag.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]); 
    storesteps();    
}


canvas.addEventListener("mousedown", startpos);
canvas.addEventListener("mouseup", endpos);
canvas.addEventListener("mousemove", draw);
canvas.addEventListener("mousedown", draw);
//currentsize.addEventListener("mousemove", sizechange);